﻿using GNBCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GNBWeb.Models
{
    public class HomeIndexViewModel
    {
        public IEnumerable<CurrencyRate> Rates { get; set; }
    }
}
