﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GNBWeb.Models
{
    public class TransactionDto
    {
        public int Id { get; set; }
        
        public string Sku { get; set; }
        
        public string Amount { get; set; }
        
        public string Currency { get; set; }
    }
}
