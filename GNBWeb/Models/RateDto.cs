﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GNBWeb.Models
{
    public class RateDto
    {
        public string From { get; set; }
        
        public string To { get; set; }
        
        public string Rate { get; set; }
    }
}
