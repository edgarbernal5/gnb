﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GNBWeb.Models
{
    public class ReportDto
    {
        public IEnumerable<TransactionDto> Transactions { get; set; }
        public string Total { get; set; }
    }
}
