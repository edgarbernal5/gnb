﻿using GNBCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GNBWeb.Models
{
    public class TransactionsViewModel
    {
        public IEnumerable<Transaction> Transactions { get; set; }

        public string Total { get; set; }
    }
}
