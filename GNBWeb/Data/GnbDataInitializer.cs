﻿using GNBCore.Entities;
using GNBData;
using GNBData.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace GNBWeb.Data
{
    public static class GnbDataInitializer
    {
        public static void Initialize(IGnbRepository repository, string jsonRates, string jsonTransactions)
        {
            repository.Initialize();
            
            if (repository.IsAnyRate())
            {
                return;
            }

            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                ContractResolver = new DefaultContractResolver()
            };

            List<CurrencyRate> rates =
             JsonConvert.DeserializeObject<List<CurrencyRate>>(
               jsonRates, settings);

            for (int i = 0; i < rates.Count; i++)
            {
                repository.AddRate(rates[i]);
            }

            List<Transaction> transactions =
             JsonConvert.DeserializeObject<List<Transaction>>(
               jsonTransactions, settings);

            for (int i = 0; i < transactions.Count; i++)
            {
                repository.AddTransaction(transactions[i]);
            }

            repository.Save();
        }
    }
    
}
