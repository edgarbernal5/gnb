using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GNBCore.Entities;
using GNBCore.Helpers;
using GNBData.Interfaces;
using GNBWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace GNBWeb.Pages.Products
{
    public class ListModel : PageModel
    {
        public TransactionsViewModel TransactionsData { get; set; }

        [BindProperty(SupportsGet = true)]
        public string FetchSku { get; set; }

        private IGnbRepository repository;

        public ListModel(IGnbRepository _repository)
        {
            repository = _repository;
        }

        public void OnGet()
        {
            var report = repository.GetReportTransactionBySku(FetchSku);

            TransactionsData = new TransactionsViewModel();
            TransactionsData.Transactions = report.Transactions;
            TransactionsData.Total = report.Total;
            
        }
    }
}