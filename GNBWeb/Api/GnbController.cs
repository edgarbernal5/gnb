﻿using AutoMapper;
using GNBCore.Entities;
using GNBCore.Helpers;
using GNBData.Interfaces;
using GNBWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GNBWeb.Api
{
    [Route("api")]
    [ApiController]
    public class GnbController : Controller
    {
        private IGnbRepository _gnbRepository;
        private ILogger<GnbController> _logger;
        private IMapper _mapper;

        public GnbController(IGnbRepository gnbRepository,
            ILogger<GnbController> logger,
            IMapper mapper)
        {
            _gnbRepository = gnbRepository;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet("rates")]
        public ActionResult<IEnumerable<CurrencyRate>> GetRates()
        {
            try
            {
                var rateEntities = _gnbRepository.GetRates();
                var results = _mapper.Map<IEnumerable<RateDto>>(rateEntities);

                return Ok(results);
            }
            catch (Exception ex)
            {
                _logger.LogCritical("Exception while getting rates.", ex);
                return StatusCode(500, "A problem happened while handling your request.");
            }
        }

        [HttpGet("transactions")]
        public ActionResult<IEnumerable<Transaction>> GetTransactions()
        {
            try
            {
                var transactionEntities = _gnbRepository.GetTransactions();
                var results = _mapper.Map<IEnumerable<TransactionDto>>(transactionEntities);

                return Ok(results);

            }
            catch (Exception ex)
            {
                _logger.LogCritical("Exception while getting transactions.", ex);
                return StatusCode(500, "A problem happened while handling your request.");
            }
        }

        [HttpGet("transactions/{sku}")]
        public ActionResult<IEnumerable<Transaction>> GetTransactions(string sku)
        {
            try
            {
                var transactionEntities = _gnbRepository.GetTransactionsBySku(sku);
                var results = _mapper.Map<IEnumerable<TransactionDto>>(transactionEntities);

                return Ok(results);

            }
            catch (Exception ex)
            {
                _logger.LogCritical($"Exception while getting transactions for sku '{sku}'.", ex);
                return StatusCode(500, "A problem happened while handling your request.");
            }
        }

        [HttpGet("report/{sku}")]
        public ActionResult<ReportDto> GetTransactionReport(string sku)
        {
            try
            {
                var report = _gnbRepository.GetReportTransactionBySku(sku);

                ReportDto reportDto = new ReportDto()
                {
                    Transactions = _mapper.Map<IEnumerable<TransactionDto>>(report.Transactions),
                    Total = report.Total
                };

                return Ok(reportDto);

            }
            catch (Exception ex)
            {
                _logger.LogCritical($"Exception while getting transactions for sku '{sku}'.", ex);
                return StatusCode(500, "A problem happened while handling your request.");
            }
        }
    }
}
