using AutoMapper;
using GNBCore.Entities;
using GNBData.Data;
using GNBData.Interfaces;
using GNBWeb.Api;
using GNBWeb.Data;
using GNBWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using Xunit;

namespace GnbUnitTest
{
    public class GnbUnitTest
    {
        private GnbController _controller;
        private IGnbRepository _repository;

        private readonly Mock<ILogger<GnbController>> _mockLogger;

        public GnbUnitTest()
        {
            _mockLogger = new Mock<ILogger<GnbController>>();

            _repository = new GnbInMemoryRepository();
            _controller = new GnbController(_repository, _mockLogger.Object, CreateMapper());
        }

        private IMapper CreateMapper()
        {
            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<GNBCore.Entities.CurrencyRate, RateDto>();
                cfg.CreateMap<GNBCore.Entities.Transaction, TransactionDto>();
            });

            return config.CreateMapper();
        }

        [Fact]
        public void GetRates_Ok_Empty()
        {
            // Act
            var okResult = _controller.GetRates().Result as OkObjectResult;
            var items = Assert.IsType<List<RateDto>>(okResult.Value);

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
            Assert.Empty(items);
        }

        [Fact]
        public void GetTransactions_Ok_Empty()
        {
            // Act
            var okResult = _controller.GetTransactions().Result as OkObjectResult;
            var items = Assert.IsType<List<TransactionDto>>(okResult.Value);

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
            Assert.Empty(items);
        }

        [Fact]
        public void GetTransactionsBySku_Ok()
        {
            Transaction t1 = new Transaction()
            {
                Id = 1,
                Sku = "G2303",
                Amount = "10.0",
                Currency = "EUR"
            };
            Transaction t2 = new Transaction()
            {
                Id = 1,
                Sku = "G2303",
                Amount = "30.0",
                Currency = "EUR"
            };
            _repository.AddTransaction(t1);
            _repository.AddTransaction(t2);

            string sku = "G2303";
            // Act
            var okResult = _controller.GetTransactions(sku).Result as OkObjectResult;
            var items = Assert.IsType<List<TransactionDto>>(okResult.Value);

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
            Assert.True(items.Count > 0);
        }


        [Fact]
        public void GetRates_Ok()
        {
            CurrencyRate r1 = new CurrencyRate()
            {
                From = "EUR",
                To = "USD",
                Rate = "0.78"
            };
            CurrencyRate r2 = new CurrencyRate()
            {
                From = "USD",
                To = "EUR",
                Rate = "1.28"
            };
            _repository.AddRate(r1);
            _repository.AddRate(r2);

            // Act
            var okResult = _controller.GetRates().Result as OkObjectResult;
            var items = Assert.IsType<List<RateDto>>(okResult.Value);

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
            Assert.True(items.Count > 0);
        }

        [Fact]
        public void GetTransactions_Ok()
        {
            Transaction t1 = new Transaction()
            {
                Id = 1,
                Sku = "G2303",
                Amount = "10.0",
                Currency = "EUR"
            };
            Transaction t2 = new Transaction()
            {
                Id = 1,
                Sku = "G2303",
                Amount = "30.0",
                Currency = "EUR"
            };
            Transaction t3 = new Transaction()
            {
                Id = 1,
                Sku = "A5503",
                Amount = "30.0",
                Currency = "EUR"
            };
            _repository.AddTransaction(t1);
            _repository.AddTransaction(t2);
            _repository.AddTransaction(t3);

            // Act
            var okResult = _controller.GetTransactions();

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
        }

        [Fact]
        public void GetReportBySku_Ok()
        {
            string jsonRates= "[{ \"from\": \"EUR\", \"to\": \"USD\", \"rate\": \"1.359\" }, { \"from\": \"CAD\", \"to\": \"EUR\", \"rate\": \"0.732\" },"+
                "{ \"from\": \"USD\", \"to\": \"EUR\", \"rate\": \"0.736\" }, { \"from\": \"EUR\", \"to\": \"CAD\", \"rate\": \"1.366\" }]";

            string jsonTransactions = "[ { \"sku\": \"T2006\", \"amount\": \"10.00\", \"currency\": \"USD\" }," +
                         "{ \"sku\": \"M2007\", \"amount\": \"34.57\", \"currency\": \"CAD\" }," +
                         "{ \"sku\": \"R2008\", \"amount\": \"17.95\", \"currency\": \"USD\" }," +
                         "{ \"sku\": \"T2006\", \"amount\": \"7.63\", \"currency\": \"EUR\" }," +
                         "{ \"sku\": \"B2009\", \"amount\": \"21.23\", \"currency\": \"USD\" }]";

            GnbDataInitializer.Initialize(_repository, jsonRates, jsonTransactions);

            string sku = "T2006";
            // Act
            var okResult = _controller.GetTransactionReport(sku).Result as OkObjectResult;
            var reportDto = Assert.IsType<ReportDto>(okResult.Value);

            var items = Assert.IsType<List<TransactionDto>>(reportDto.Transactions);

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
            Assert.True(items.Count > 0);
            Assert.Equal("14.99", reportDto.Total);
        }

        [Fact]
        public void GetReportBySku_Empty()
        {
            string sku = "T2006";
            // Act
            var okResult = _controller.GetTransactionReport(sku).Result as OkObjectResult;
            var reportDto = Assert.IsType<ReportDto>(okResult.Value);

            var items = Assert.IsType<List<TransactionDto>>(reportDto.Transactions);

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
            Assert.Empty(items);
            Assert.Equal("0.00", reportDto.Total);
        }
    }
}
