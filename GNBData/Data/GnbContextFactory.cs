﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace GNBData.Data
{
    public class GnbContextFactory : IDesignTimeDbContextFactory<GnbDbContext>
    {
        public GnbDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<GnbDbContext>();
            optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=Gnb;Trusted_Connection=True;MultipleActiveResultSets=true");

            return new GnbDbContext(optionsBuilder.Options);
        }
    
    }
}
