﻿using GNBCore.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace GNBData
{
    public class GnbDbContext : DbContext
    {
        public DbSet<CurrencyRate> Rates { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        public GnbDbContext(DbContextOptions<GnbDbContext> options)
           : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CurrencyRate>()
                .HasKey(c => new { c.From, c.To });
        }
    }
}
