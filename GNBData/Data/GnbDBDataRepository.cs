﻿using GNBCore.Entities;
using GNBCore.Helpers;
using GNBData.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNBData.Data
{
    public class GnbDbDataRepository : IGnbRepository
    {
        private GnbDbContext _context;

        public GnbDbDataRepository(GnbDbContext context)
        {
            _context = context;
        }

        public void AddRate(CurrencyRate rate)
        {
            _context.Add(rate);
        }

        public void AddTransaction(Transaction transaction)
        {
            _context.Add(transaction);
        }

        public IEnumerable<CurrencyRate> GetRates()
        {
            return _context.Rates;
        }

        public Report GetReportTransactionBySku(string sku)
        {
            Report report = new Report();
            var rates = GetRates().ToList();
            var transactions = _context.Transactions.Where(t => t.Sku == sku).Select
                (t =>
                new Transaction { Id = t.Id, Currency = "EUR", Sku = t.Sku, Amount = CurrencyHelper.ConvertAmount(t.Currency, "EUR", t.Amount, rates) }
                ).ToList();

            report.Transactions = transactions;

            decimal total = 0;
            foreach (var item in transactions)
            {
                decimal amount = decimal.Parse(item.Amount);
                total += amount;
            }

            //bank = Math.Round(newAmount);
            report.Total = String.Format("{0:0.00}", total);

            return report;
        }

        public IEnumerable<Transaction> GetTransactions()
        {
            return _context.Transactions.OrderBy(t => t.Sku);
        }

        public IEnumerable<Transaction> GetTransactionsBySku(string sku)
        {
            return _context.Transactions.Where(t => t.Sku == sku);
        }

        public bool Initialize()
        {
            return _context.Database.EnsureCreated();
        }

        public bool IsAnyRate()
        {
            return _context.Rates.Any();
        }

        public bool IsAnyTransaction()
        {
            return _context.Transactions.Any();
        }

        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }
    }
}
