﻿using GNBCore.Entities;
using GNBCore.Helpers;
using GNBData.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNBData.Data
{
    public class GnbInMemoryRepository : IGnbRepository
    {
        
        public void AddRate(CurrencyRate rate)
        {
            GnbDataStore.Current.Rates.Add(rate);
        }

        public void AddTransaction(Transaction transaction)
        {
            GnbDataStore.Current.Transactions.Add(transaction);
        }

        public IEnumerable<CurrencyRate> GetRates()
        {
            return GnbDataStore.Current.Rates;
        }

        public Report GetReportTransactionBySku(string sku)
        {
            Report report = new Report();
            var rates = GetRates().ToList();
            var transactions = GnbDataStore.Current.Transactions.Where(t => t.Sku == sku).Select
                (t =>
                new Transaction { Id = t.Id, Currency = "EUR", Sku = t.Sku, Amount = CurrencyHelper.ConvertAmount(t.Currency, "EUR", t.Amount, rates) }
                ).ToList();

            report.Transactions = transactions;

            decimal total = 0;
            foreach (var item in transactions)
            {
                decimal amount = decimal.Parse(item.Amount);
                total += amount;
            }

            //bank = Math.Round(newAmount);
            report.Total = String.Format("{0:0.00}", total);

            return report;
        }

        public IEnumerable<Transaction> GetTransactions()
        {
            return GnbDataStore.Current.Transactions;
        }

        public IEnumerable<Transaction> GetTransactionsBySku(string sku)
        {
            return GnbDataStore.Current.Transactions.Where(t => t.Sku == sku);
        }

        public bool Initialize()
        {
            return true;
        }

        public bool IsAnyRate()
        {
            return GnbDataStore.Current.Rates.Any();
        }

        public bool IsAnyTransaction()
        {
            return GnbDataStore.Current.Transactions.Any();
        }

        public bool Save()
        {
            return true;
        }
    }
}
