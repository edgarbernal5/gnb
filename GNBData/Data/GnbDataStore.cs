﻿using GNBCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace GNBData.Data
{
    public class GnbDataStore
    {
        public static GnbDataStore Current { get; } = new GnbDataStore();
        public List<CurrencyRate> Rates { get; set; } = new List<CurrencyRate>();
        public List<Transaction> Transactions { get; set; } = new List<Transaction>();
        
    }
}
