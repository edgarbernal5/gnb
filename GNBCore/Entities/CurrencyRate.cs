﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GNBCore.Entities
{
    [Table("Rates")]
    public class CurrencyRate
    {
        [Required]
        public string From { get; set; }

        [Required]
        public string To { get; set; }

        [Required]
        public string Rate { get; set; }
    }
}
