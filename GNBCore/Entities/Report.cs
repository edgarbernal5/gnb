﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GNBCore.Entities
{
    public class Report
    {
        public IEnumerable<Transaction> Transactions { get; set; }
        public string Total { get; set; }
    }
}
