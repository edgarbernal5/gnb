﻿using GNBCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GNBCore.Helpers
{
    public static class CurrencyHelper
    {
        private struct State
        {
            public string node;
            public decimal ratio;

            public State(string _node, decimal _ratio)
            {
                node = _node;
                ratio = _ratio;
            }
        }

        public static string ConvertAmount(string from, string to, string amount, List<CurrencyRate> rates)
        {
            if (from == to)
            {
                return amount;
            }

            Queue<State> q = new Queue<State>();
            q.Enqueue(new State(from, 1));

            Dictionary<string, bool> visited = new Dictionary<string, bool>();
            visited[from] = true;

            while (q.Count > 0)
            {
                State current = q.Dequeue();

                if (current.node == to)
                {
                    decimal newAmount = decimal.Parse(amount) * current.ratio;
                    //newAmount = Math.Round(newAmount);

                    return String.Format("{0:0.00}", newAmount);
                }

                foreach (var rate in rates)
                {
                    if (rate.From == current.node && !visited.ContainsKey(rate.To))
                    {
                        visited[rate.To] = true;

                        decimal newRatio = decimal.Parse(rate.Rate) * current.ratio;
                        q.Enqueue(new State(rate.To, newRatio));
                    }
                }
            }

            return string.Empty;
        }
    }
}
