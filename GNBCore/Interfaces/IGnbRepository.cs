﻿using GNBCore;
using GNBCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace GNBData.Interfaces
{
    public interface IGnbRepository
    {
        bool IsAnyRate();
        bool IsAnyTransaction();

        void AddRate(CurrencyRate rate);
        void AddTransaction(Transaction transaction);

        IEnumerable<CurrencyRate> GetRates();
        IEnumerable<Transaction> GetTransactions();
        IEnumerable<Transaction> GetTransactionsBySku(string sku);
        Report GetReportTransactionBySku(string sku);

        bool Initialize();
        bool Save();
    }
}
